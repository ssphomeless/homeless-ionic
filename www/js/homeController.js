/**
 * Created by tongchunyin on 16/9/15.
 */
angular.module('starter.homeController', ['ionic'])

  .controller('homeController', function($scope, $ionicLoading, $ionicPopup, $state, $window) {

    $scope.$on('$ionicView.enter', function() {
      $scope.userName = AV.User.current().get('displayName');

      var userRole = AV.User.current().get('userRole');
      if(userRole === 'accountant') {
        $scope.items = ["查詢舍友", "增加舍友", "欠款舍友", "匯出資料", "付款批核"];
      }
      else {
        $scope.items = ["查詢舍友", "增加舍友", "欠款舍友", "匯出資料"];
      }
    });

    $scope.rowPressed = function (index) {

      //para pass
      switch(index) {
        case 0:
          $state.go('search');
          break;
        case 1:
          $state.go("userProfile");
          break;
        case 2:
          $state.go("netgativeUser");
          break;
        case 3:
          $state.go("exportData");
          break;
        case 4:
          $state.go("paymentApproval");
          break;

        default:
      }

    };

    $scope.logoutPressed = function()
    {
        window.localStorage.removeItem("username");
        window.localStorage.removeItem("password");
        $state.go('login');
    };
  });
;
