/**
 * Created by tongchunyin on 17/9/15.
 */
/**
 * Created by tongchunyin on 16/9/15.
 */
angular.module('starter.userProfileController', ['ionic', 'ngCordova.plugins.file'])

  .controller('searchNetgativeUserController', function($scope, $ionicLoading, $ionicPopup, $state, $stateParams, $timeout) {

    $scope.searchResult = [];

    $timeout(function() {

      $ionicLoading.show({
        template: 'Loading...'
      });

      AV.Cloud.run("queryNetgativeMember", {}).then(function(results) {

        console.log("query netgative user result: " + results.length);

        $ionicLoading.hide();

        $scope.searchResult = results;
        $scope.searchResultOriginal = results;

      }, function(error) {

        $ionicLoading.hide();
        console.log("error occur");
      });


    });


    $scope.userDetailPressed = function (index)
    {
      var people = $scope.searchResult[index];
      $state.go("userProfile", { people: people }); //passing para
      //$state.go("userProfile");
    }



  })

    .controller('userProfileController', function($scope, Camera, $cordovaFile, $ionicLoading, $ionicPopup, $state, $stateParams, $timeout) {

    $scope.searchName = '';

    $scope.title = '';

    $scope.initPara = function ()
    {
      $scope.rooms = [];

      var RoomClass = AV.Object.extend("Room");
      var queryRoom = new AV.Query(RoomClass);
      queryRoom.find({
        success: function (results) {
          $scope.rooms = results;
          $scope.$apply();
        },
        error: function (error) {

        }

      });

    };

    $timeout(function(){
      // Any code in here will automatically have an $scope.apply() run afterwards
      $scope.searchPeople = $stateParams.people;

      console.log('searched people ID: ' + $stateParams.people.id)
      // And it just works!
      var navTitle = angular.element(document.querySelector('#navTitle'));

      if($scope.searchPeople != null)
      {//update mode

        navTitle.text('更新舍友');

        var addArea =  angular.element(document.querySelector('#addArea'));
        //addButton.style.display ='none';
        addArea.css("display","none");

        $ionicLoading.show({
          template: 'Loading...'
        });

        console.log('searched people ID2: ' + $scope.searchPeople.id)

        var People = AV.Object.extend("People");
        var queryPeople = new AV.Query(People);
        queryPeople.include('roomRef');
        queryPeople.include('accountRef');
        queryPeople.get($scope.searchPeople.id, {
          success: function (targetPeople) {
            $ionicLoading.hide();
            console.log(targetPeople.get('outDate'));

            $scope.people = $scope.convertParseObjectIntoAngularPeople(targetPeople);
            $scope.$apply();

            console.log($scope.people);

          },
          error: function(error) {

          }
        });
        }
      else {
        //insert mode
        navTitle.text('新增舍友');

        var People = AV.Object.extend("People");
        var people = new People();
        $scope.people = people;

        var editArea =  angular.element(document.querySelector('#editArea'));
        editArea.css("display","none");
      }


    });


    //search function
    $scope.searchUser = function ()
    {
      $ionicLoading.show({
        template: 'Loading...'
      });

      var People = AV.Object.extend("People");
      var query = new AV.Query(People);
      query.contains("userName", $scope.searchName);
      query.include('accountRef');
      query.find({
        success: function(results) {
          // results has the list of users with a hometown team with a winning record
          $ionicLoading.hide();

          $scope.searchResultOriginal = results;
          $scope.searchResult = results.map(function (obj) {
            var birth = moment(obj.get("birth")).format('YYYY-MM-DD');
            return {
              birth: birth,
              userName: obj.get("userName")
            };
          });

        }
      });
    };

    $scope.userDetailPressed = function (index)
    {
      var people = $scope.searchResultOriginal[index];
      $state.go("userProfile", { people: people }); //passing para
    }




    $scope.userValidation = function()
      {

        if($scope.people.userName === undefined || $scope.people.userName.length == 0)
        {
          $scope.showMessage('錯誤', '請輸入名稱');
          return false;
        }
        else if($scope.people.birth === undefined || $scope.people.birth.length == 0)
        {
          $scope.showMessage('錯誤', '請輸入出生日期');
          return false;
        }
        else if($scope.people.room === undefined || $scope.people.room.length == 0)
        {
          $scope.showMessage('錯誤', '請輸入宿舍地點');
          return false;
        }

        //for setting false for never touched parameter
        if($scope.people.hasAssist == undefined)
        {
          $scope.people.hasAssist = false;
        }
        if($scope.people.hasBodyCheck == undefined)
        {
          $scope.people.hasBodyCheck = false;
        }

        if(!$scope.people.outDate|| $scope.people.outDate.length == 0)
        {

        }
        else
        {
          //not empty
           var inDate = moment($scope.people.inDate).format('YYYY-MM-DD');
           var outDate = moment($scope.people.outDate).format('YYYY-MM-DD');

          if(inDate > outDate)
          {
             $scope.showMessage('錯誤', '離開日期不能少於入住日期');
            return false;
          }
        }

          return true;
      };


      function savePhotoToParse(photo)
      {
        if(photo.startsWith('http'))
        {
          return new Promise(function(resolve, reject) {
            resolve($scope.people.picture);
          });
        }
        else {
          return Camera.toBase64Image(photo).then(function (result)
          {
            var base64Photo = result.imageData;

            // create the parse file object using base64 representation of photo
            var imageFile = new AV.File("mypic.jpg", {base64: base64Photo});

            // save the parse file object
            return imageFile.save().then(function () {
              base64Photo = null;
              return imageFile;
            });
          });
        }

      };

      function savePeopleWhenAdd(parseObject)
      {
        parseObject.save(null, {
          success: function (obj) {
            $ionicLoading.hide();

            // Execute any logic that should take place after the object is saved.
            var msg = '成功建立舍友';
            $scope.showMessage('Success', msg);

            $state.go('home');
          },
          error: function (obj, error) {
            // Execute any logic that should take place if the save fails.
            // error is a Parse.Error with an error code and message.
            $ionicLoading.hide();
            $scope.showMessage('錯誤', error.message);
          }
        });
      }

      $scope.addUserPressed = function() {
        var valid = $scope.userValidation();
        if (!valid) {
          return;
        }

        var parseObject = $scope.convertAngularPeopleIntoParseObject(null, $scope.people);

        $ionicLoading.show({
          template: 'Saving...'
        });


        if($scope.lastPhoto)
        {
           savePhotoToParse($scope.lastPhoto).then(function(imageFile){
            parseObject.set("picture", imageFile);
            savePeopleWhenAdd(parseObject);
          });
        }
        else
        {
          savePeopleWhenAdd(parseObject);
        }
    };

    function savePeopleWhenEdit(parseObject)
    {
      parseObject.save(null, {
        success: function (obj) {

          $ionicLoading.hide();

          // Execute any logic that should take place after the object is saved.
          var msg = '成功更新舍友';
          $scope.showMessage('Success', msg);

          $state.go('home');
        },
        error: function (obj, error) {

          $ionicLoading.hide();
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          $scope.showMessage('錯誤', error.message);
        }
      });
    }

    $scope.editUserPressed = function() {

      var confirmPopup = $ionicPopup.confirm({
        title: '更新',
        template: '確定要更新舍友?'
      });
      confirmPopup.then(function(res) {
        if(res) {
          var valid = $scope.userValidation();
          if (!valid) {
            return;
          }

          var parseObject = $scope.convertAngularPeopleIntoParseObject($scope.searchPeople, $scope.people);

          $ionicLoading.show({
            template: 'Saving...'
          });

          if($scope.lastPhoto)
          {
             savePhotoToParse($scope.lastPhoto).then(function(imageFile){
              parseObject.set("picture", imageFile);
              savePeopleWhenEdit(parseObject);
            });
          }
          else
          {
            savePeopleWhenEdit(parseObject);
          }

        } else {
        }
      });

    };


    $scope.deleteUserPressed = function()
    {
      var confirmPopup = $ionicPopup.confirm({
        title: '移除',
        template: '確定要移除舍友嗎?'
      });
      confirmPopup.then(function(res) {
        if(res) {
          var confirmPopup = $ionicPopup.confirm({
            title: '警告',
            template: '一經移除,資料無法再復完,確定要繼續?'
          });
          confirmPopup.then(function(res) {
            if(res) {

              $ionicLoading.show({
                template: 'Deleting...'
              });

              $scope.searchPeople.destroy({
                success: function(myObject) {

                  $ionicLoading.hide();

                  // The object was deleted from the Parse Cloud.
                  var msg = '成功移除舍友';
                  $scope.showMessage('Success', msg);

                  $state.go('home');
                },
                error: function(myObject, error) {

                  $ionicLoading.hide();

                  // The delete failed.
                  // error is a Parse.Error with an error code and message.
                  $scope.showMessage('錯誤', error.message);
                }
              });
            } else {
            }
          });
        } else {
        }
      });

    };

    $scope.feeListPressed = function()
    {
      $state.go("userFee", { people: $scope.searchPeople }); //passing para
    }

    $scope.payListPressed = function()
    {
      $state.go("userPayment", { people: $scope.searchPeople }); //passing para
    }

    $scope.paymentPressed = function()
    {
      $state.go("collectFee", { people: $scope.searchPeople }); //passing para
    }

    $scope.newFeePressed = function()
    {
      $state.go("createFee", { people: $scope.searchPeople }); //passing para
    }



    $scope.showMessage = function(title, msg) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: msg
      });
      alertPopup.then(function(res) {
      });
    };

    $scope.convertAngularPeopleIntoParseObject = function(subject, targetObj)
    {
      var People = AV.Object.extend("People");
      var people ;
      if(subject == null)
      {
        people = new People();
      }
      else {
        people = subject;
      }

      people.set('userName', targetObj.userName);
      people.set('birth', targetObj.birth);
      people.set('room', targetObj.room);
      people.set('inDate', targetObj.inDate);
      people.set('outDate', targetObj.outDate);
      people.set('hasBodyCheck', targetObj.hasBodyCheck);
      people.set('hasAssist', targetObj.hasAssist);

      return people;
    }

    $scope.convertParseObjectIntoAngularPeople = function(targetObj)
    {
      var People = AV.Object.extend("People");
      var people = new People();

      people.userName = targetObj.get('userName');
      people.birth = targetObj.get('birth');
      people.room = targetObj.get('room');
      people.inDate = targetObj.get('inDate');
      people.outDate = targetObj.get('outDate');
      people.hasBodyCheck = targetObj.get('hasBodyCheck');
      people.hasAssist = targetObj.get('hasAssist');
      people.runningBalance = targetObj.get('accountRef').get('runningBalance');
      people.picture = targetObj.get('picture');

      if(people.picture)
      {
        $scope.lastPhoto = people.picture.url();
      }

      return people;
    }

  /** ---------------------------------------------------------------------
   *
   * display alert to choose where to get the image from
   *
   --------------------------------------------------------------------- */
  $scope.getPhoto = function () {
      var options = {
          'buttonLabels': ['Take Picture', 'Select From Gallery'],
          'addCancelButtonWithLabel': 'Cancel'
      };
      window.plugins.actionsheet.show(options, callback);
  };

  function callback(buttonIndex) {
      console.log(buttonIndex);
      if (buttonIndex === 1) {

          var picOptions = {
              destinationType: navigator.camera.DestinationType.FILE_URI,
              quality: 75,
              targetWidth: 500,
              targetHeight: 500,
              allowEdit: true,
              saveToPhotoAlbum: false
          };


          Camera.getPicture(picOptions).then(function (imageURI) {
              console.log(imageURI);
              $scope.lastPhoto = imageURI;
              $scope.newPhoto = true;

          }, function (err) {
              console.log(err);
              $scope.newPhoto = false;
              alert(err);
          });
      } else if (buttonIndex === 2) {

          var picOptions = {
              destinationType: navigator.camera.DestinationType.FILE_URI,
              quality: 75,
              targetWidth: 500,
              targetHeight: 500,
              sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
          };


          Camera.getPictureFromGallery(picOptions).then(function (imageURI) {
              console.log(imageURI);
              $scope.lastPhoto = imageURI;
              $scope.newPhoto = true;

          }, function (err) {
              console.log(err);
              $scope.newPhoto = false;
              alert(err);
          });
      }

  };

  });




