/**
 * Created by tongchunyin on 22/10/15.
 */


angular.module('starter.userFeeController', ['ionic'])

  .controller('userFeeController', function($scope, $ionicLoading, $ionicPopup, $state, $stateParams, $timeout, $ionicHistory) {

    //Work around: for fixing the parameter not updating problems
    $scope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
        $ionicHistory.backView().stateParams = fromParams
    });


    $timeout(function() {
      // Any code in here will automatically have an $scope.apply() run afterwards
      $scope.people = $stateParams.people;

      if($scope.people != null) {
        var FeeClass = AV.Object.extend("Fee");
        var queryFee = new AV.Query(FeeClass);
        queryFee.equalTo("owner", $scope.people);
        queryFee.include("feeTypeRef");
        queryFee.descending("createdAt");

        $ionicLoading.show({
          template: 'Loading...'
        });

        queryFee.find({
          success: function (results) {
            // results has the list of users with a hometown team with a winning record

            $ionicLoading.hide();

            $scope.feeLists = results.map(function (obj) {

              var createDate = moment(obj.createdAt).format('YYYY-MM-DD');
              var paymentDate = '';
              if (obj.get("settledDate") != null)
              {
                paymentDate = moment(obj.get("settledDate")).utcOffset(8).format('YYYY-MM-DD HH:mm A');
              }

              return {
                createDate: createDate,
                amount: obj.get("amount"),
                settledAmount: obj.get("settledAmount"),
                type : obj.get("feeTypeRef").get("name"),
                settleDate : paymentDate,
                reference: obj.get("reference")
              };
            });

            $scope.$apply();
          }
        });
      }

    });

  })

  .controller('collectFeeController', function($scope, $ionicLoading, $ionicPopup, $state, $stateParams, $timeout, $ionicHistory) {

    //Work around: for fixing the parameter not updating problems
    $scope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
        $ionicHistory.backView().stateParams = fromParams
    });

    $timeout(function() {
      // Any code in here will automatically have an $scope.apply() run afterwards
      $scope.people = $stateParams.people;

      var PaymentClass = AV.Object.extend("Payment");
      var payment= new PaymentClass();
      $scope.payment = payment;

    });


    $scope.feeValidation = function()
    {

      if($scope.payment.amount === undefined || $scope.payment.amount <= 0)
      {
        $scope.showMessage('錯誤', '請輸入正確金額');
        return false;
      }
      else if($scope.payment.reference === undefined || $scope.payment.reference.length == 0) {
        $scope.showMessage('錯誤', '請輸入備注');
        return false;
      }

      return true;
    };



    $scope.collectFeePressed = function() {

      var valid = $scope.feeValidation();
      if (!valid) {
        return;
      }

      var parseObject = $scope.convertAngularPaymentIntoParseObject(null, $scope.payment);
      parseObject.set('owner', $scope.people);
      parseObject.set('creator', AV.User.current());
      parseObject.set('isApproved', false);

      $ionicLoading.show({
        template: 'Saving...'
      });


      parseObject.save(null, {
        success: function (obj) {

          $ionicLoading.hide();

          // Execute any logic that should take place after the object is saved.
          var msg = '成功收取付款';
          $scope.showMessage('Success', msg);

          $ionicHistory.goBack();
          //$state.go('home');
        },
        error: function (obj, error) {

          $ionicLoading.hide();

          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          $scope.showMessage('錯誤', error.message);
        }
      });
    }

    $scope.convertAngularPaymentIntoParseObject = function(subject, targetObj)
    {
      var PaymentClass = AV.Object.extend("Payment");
      var payment;
      if(subject == null)
      {
        payment= new PaymentClass();
      }
      else {
        payment= subject;
      }

      payment.set('amount', targetObj.amount);
      payment.set('reference', targetObj.reference);

      return payment;
    }


    $scope.showMessage = function(title, msg) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: msg
      });
      alertPopup.then(function(res) {
      });
    };

  })


  .controller('createFeeController', function($scope, $ionicLoading, $ionicPopup, $state, $stateParams, $timeout, $ionicHistory) {

    //Work around: for fixing the parameter not updating problems
    $scope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
      $ionicHistory.backView().stateParams = fromParams
    });

    $timeout(function() {
      // Any code in here will automatically have an $scope.apply() run afterwards
      $scope.people = $stateParams.people;

      var FeeClass = AV.Object.extend("Fee");
      var fee= new FeeClass();
      $scope.fee= fee;

    });

    $scope.initPara = function()
    {
      var FeeTypeClass = AV.Object.extend("FeeType");
      var queryFeeType = new AV.Query(FeeTypeClass);
      queryFeeType.ascending("priority");
      queryFeeType.find({
        success: function (results) {
          $scope.feetypes = results;
        },
        error: function (error) {

        }
      });
    };


    $scope.feeValidation = function()
    {

      if($scope.fee.amount === undefined || $scope.fee.amount <= 0)
      {
        $scope.showMessage('錯誤', '請輸入正確金額');
        return false;
      }
      else if($scope.fee.feeType === undefined || $scope.fee.feeType.length == 0) {
        $scope.showMessage('錯誤', '請輸入收費種類');
        return false;
      }

      return true;
    };



    $scope.createFeePressed = function() {

      var valid = $scope.feeValidation();
      if (!valid) {
        return;
      }

      var parseObject = $scope.convertAngularFeeIntoParseObject(null, $scope.fee);

      parseObject.set('owner', $scope.people);
      parseObject.set('creator', AV.User.current());
      parseObject.set('settledAmount', 0);
      parseObject.set('isSettled', false);

      $ionicLoading.show({
        template: 'Saving...'
      });


      parseObject.save(null, {
        success: function (obj) {

          $ionicLoading.hide();

          // Execute any logic that should take place after the object is saved.
          var msg = '成功新增收費';
          $scope.showMessage('Success', msg);

          $ionicHistory.goBack();
          //$state.go('home');
        },
        error: function (obj, error) {

          $ionicLoading.hide();

          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          $scope.showMessage('錯誤', error.message);
        }
      });
    }

    $scope.convertAngularFeeIntoParseObject = function(subject, targetObj)
    {
      var FeeClass = AV.Object.extend("Fee");
      var fee;
      if(subject == null)
      {
        fee= new FeeClass();
      }
      else {
        fee= subject;
      }

      fee.set('amount', targetObj.amount);
      fee.set('feeType', targetObj.feeType);
      fee.set('reference', targetObj.reference);

      return fee;
    }


    $scope.showMessage = function(title, msg) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: msg
      });
      alertPopup.then(function(res) {
      });
    };

  })


  .controller('userPaymentController', function($scope, $ionicLoading, $ionicPopup, $state, $stateParams, $timeout, $ionicHistory) {

    //Work around: for fixing the parameter not updating problems
    $scope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
      $ionicHistory.backView().stateParams = fromParams
    });

    $timeout(function() {
      // Any code in here will automatically have an $scope.apply() run afterwards
      $scope.people = $stateParams.people;

      if($scope.people != null) {
        var PaymentClass = AV.Object.extend("Payment");
        var queryPayment = new AV.Query(PaymentClass);
        queryPayment.equalTo("owner", $scope.people);
        queryPayment.descending("createdAt");

        $ionicLoading.show({
          template: 'Loading...'
        });

        queryPayment.find({
          success: function (results) {
            // results has the list of users with a hometown team with a winning record

            $ionicLoading.hide();

            $scope.paymentLists = results.map(function (obj) {

              var createDate = moment(obj.createdAt).utcOffset(8).format('YYYY-MM-DD HH:mm A');
              var creator = obj.get('creator').get('displayName');

              var approved =  obj.get('isApproved') ? "已批核" : "待批核";

              return {
                createDate: createDate,
                amount: obj.get("amount"),
                creator: creator,
                approved: approved
              };
            });

            $scope.$apply();
          }
        });
      }

    });

  })


  .controller('paymentApprovalController', function($scope, $ionicLoading, $ionicPopup, $state, $stateParams, $timeout) {

    $scope.initPayment = function()
    {
      var PaymentClass = AV.Object.extend("Payment");
      var queryPayment = new AV.Query(PaymentClass);
      queryPayment.equalTo("isApproved", false);
      queryPayment.include("owner");
      queryPayment.include("creator");
      queryPayment.descending("createdAt");

      $ionicLoading.show({
        template: 'Loading...'
      });


      queryPayment.find({
        success: function (results) {
          // results has the list of users with a hometown team with a winning record
          $ionicLoading.hide();

          $scope.approvalLists = results.map(function (obj) {

            var createDate = moment(obj.createdAt).utcOffset(8).format('YYYY-MM-DD HH:mm A');
            var owner = obj.get("owner").get("userName");
            var collector = obj.get("creator").get("displayName");

            return {
              id: obj.id,
              createDate: createDate,
              amount: obj.get("amount"),
              status: "2",
              owner: owner,
              collector: collector,
              reference: obj.get("reference")
            };
          });
        }
      });
    };

    $scope.approveSelection = function()
    {
      var confirmPopup = $ionicPopup.confirm({
        title: '確定',
        template: '確定要批核付款嗎?'
      });
      confirmPopup.then(function(res) {
        if(res) {
          var approveList = [];
          var rejectList = [];

          for(var i = 0 ; i < $scope.approvalLists.length ; i++)
          {
            var payment = $scope.approvalLists[i];

            if(payment.status === "1")
            {
              approveList.push(payment.id);
            }
            else if(payment.status === "3")
            {
              rejectList.push(payment.id);
            }
          }

          if(approveList.length > 0 || rejectList.length > 0)
          {
            AV.Cloud.run("approvePayment", { approveList:approveList, rejectList:rejectList }).then(function(result) {
              // make sure the set the enail sent flag on the object
              var msg = '成功批核';
              $scope.showMessage('Success', msg);
              $state.go('home');

            }, function(error) {
              // error
              $scope.showMessage('錯誤', error.message);
              $state.go('home');

            });
          }
          else {
            $scope.showMessage('錯誤', '請選擇最少一個項目來批核!');
          }

        } else {
        }
      });
    }


    $scope.showMessage = function(title, msg) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: msg
      });
      alertPopup.then(function(res) {
      });
    };

  })



;
