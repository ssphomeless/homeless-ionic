/**
 * Created by tongchunyin on 17/9/15.
 */
/**
 * Created by tongchunyin on 16/9/15.
 */
angular.module('starter.exportDataController', ['ionic', 'ngCordova.plugins.file'])

    .controller('exportDataController', function($scope, Camera, $cordovaFile, $ionicLoading, $ionicPopup, $state, $stateParams, $timeout) {

    $scope.email = window.localStorage.getItem("email");
    if(!$scope.email )
    {
      $scope.email = '';
    }

    $scope.exportData = function() {

      if($scope.email.length == 0)
      {
        $scope.showMessage('error', '請輸入Email address.');
        return;
      }

      window.localStorage.setItem("email", $scope.email);

      var filePaths = [];

      $scope.exportPayment(function(filePath)
      {
        filePaths.push(filePath);

        $scope.exportFee(function(filePath)
        {
          filePaths.push(filePath);

          $scope.exportPeople(function(filePath)
          {
            filePaths.push(filePath);
            $scope.sendEmail(filePaths);

          }, function(error)
          {
            $scope.showMessage('error', error);
          });
        }, function(error)
        {
          $scope.showMessage('error', error);
        });
      }, function(error)
      {
        $scope.showMessage('error', error);
      });
    };

    $scope.exportPayment = function(success, error){

        var PaymentClass = AV.Object.extend("Payment");
        var queryPayment = new AV.Query(PaymentClass);
        queryPayment.include('owner');
        queryPayment.include('creator');
        queryPayment.descending("createdAt");

        $ionicLoading.show({
          template: 'Loading...'
        });

        queryPayment.find({
          success: function (results) {
            // results has the list of users with a hometown team with a winning record

            $ionicLoading.hide();

            var data = "";
            //loop as csv

            console.log("results begin");
            console.log(results);

            for(var i=0; i<results.length; i++)
            {
              var obj = results[i];
              //該月已付款的舍友姓名、付款金額、付款類別、收據編號、付款日期及收款人姓名
              var name = obj.get('owner').get('userName');
              var amount= obj.get("amount");
              var reference= obj.get("reference");
              var createDate = moment(obj.createdAt).utcOffset(8).format('YYYY-MM-DD HH:mm A');
              var recipient = obj.get('creator').get('displayName');
              var approved =  obj.get('isApproved') ? "已批核" : "待批核";
              var paymentDetail =  obj.get('paymentDetail');

              data += name +","+ amount +","+ reference +","+ createDate +","+ recipient +","+ approved + ","  + paymentDetail + '\n';
            }

            $scope.saveToFile('payment.csv', data, success, error);
          }
        });
    };

    $scope.exportFee = function(success, error){

        var targetClass = AV.Object.extend("Fee");
        var queryPayment = new AV.Query(targetClass);
        queryPayment.include('owner');
        queryPayment.include('feeTypeRef');
        queryPayment.descending("createdAt");

        $ionicLoading.show({
          template: 'Loading...'
        });

        queryPayment.find({
          success: function (results) {
            // results has the list of users with a hometown team with a winning record

            $ionicLoading.hide();

            var data = "";
            //loop as csv

            console.log("results begin");
            console.log(results);

            for(var i=0; i<results.length; i++)
            {
              var obj = results[i];
              //該月已付款的舍友姓名、付款金額、付款類別、收據編號、付款日期及收款人姓名
              var name = obj.get('owner').get('userName');
              var feeType = obj.get('feeTypeRef').get('name');
              var amount= obj.get("amount");
              var reference= obj.get("reference");
              var createDate = moment(obj.createdAt).utcOffset(8).format('YYYY-MM-DD HH:mm A');
              var settled =  obj.get('isSettled') ? "已繳費" : "待繳費";
              var settledAmt =  obj.get('settledAmount');


              data += name +","+ feeType + "," +amount +","+ reference +","+ createDate +","+ settled +","+ settledAmt + '\n';
            }


            $scope.saveToFile('fee.csv', data, success, error);
          }
        });
    };

    $scope.exportPeople = function(success, error){

        var targetClass = AV.Object.extend("People");
        var queryPayment = new AV.Query(targetClass);
        queryPayment.include('accountRef');
        queryPayment.include('roomRef');
        queryPayment.descending("createdAt");

        $ionicLoading.show({
          template: 'Loading...'
        });

        queryPayment.find({
          success: function (results) {
            // results has the list of users with a hometown team with a winning record

            $ionicLoading.hide();

            var data = "";
            //loop as csv

            console.log("results begin");
            console.log(results);

            for(var i=0; i<results.length; i++)
            {
              var obj = results[i];
              //該月已付款的舍友姓名、付款金額、付款類別、收據編號、付款日期及收款人姓名
              var name = obj.get('userName');
              var birth = moment(obj.get("birth")).utcOffset(8).format('YYYY-MM-DD');
              var assisted =  obj.get('hasAssist') ? "有資助" : "沒有資助";
              var bodyChecked =  obj.get('hasBodyCheck') ? "已身體檢查" : "沒有身體檢查";
              var accountBal = obj.get('accountRef').get('runningBalance');
              var room = obj.get('roomRef').get('name');
              var assistPrice = obj.get('roomRef').get('assistPrice');
              var nonAssistPrice = obj.get('roomRef').get('nonAssistPrice');

              data += name +","+ birth + "," +assisted +","+ bodyChecked +","+ accountBal +","+ room +","+ assistPrice +","+ nonAssistPrice + '\n';
            }

            $scope.saveToFile('people.csv', data, success, error);
          }
        });
    };

    $scope.saveToFile = function(fileName, data, success, error)
    {
      var filename = fileName;
      var filePath = cordova.file.dataDirectory;

      var isAndroid = ionic.Platform.isAndroid();
      if(isAndroid)
      {
        filePath = cordova.file.externalDataDirectory;
      }

      $cordovaFile.writeFile(filePath, filename, data, true)
        .then(function (result) {

          console.log(JSON.stringify(result));
          console.log(result.target.localURL);
          console.log(cordova.file.applicationStorageDirectory);
          console.log(cordova.file.dataDirectory);
          console.log(cordova.file.externalRootDirectory);
          console.log(cordova.file.externalApplicationStorageDirectory);
          console.log(cordova.file.externalDataDirectory);

          var finalPath = filePath.substring(8);
          // var finalPath = filePath.replace('file://', '');
          var finalFilePath = finalPath+filename;

          // var finalFilePath = $scope.urlForImage(success.target.localURL);

          success(finalFilePath);
        }, function (err) {

          error(err);
        });

    };

    $scope.sendEmail = function (filePaths)
    {
      console.log('file path: ' + filePaths);

      if(window.plugins && window.plugins.emailComposer) {
        window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
            console.log("Response -> " + result);
          },
          "Data Export", // Subject
          "This is auto generated document.",                      // Body
          [$scope.email],    // To
          null,                    // CC
          null,                    // BCC
          false,                   // isHTML
          filePaths,                    // Attachments
          null);                   // Attachment Data
      }
    };


    $scope.showMessage = function(title, msg) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: msg
      });
      alertPopup.then(function(res) {
      });
    };

  });




