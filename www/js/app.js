// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in loginController.js
var angularModule = angular.module('starter', ['ionic', 'starter.loginController', 'starter.homeController', 'starter.userProfileController', 'starter.userFeeController', 'starter.exportDataController']);

angularModule.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }

    //UAT config:
    //AV.initialize('XbicVrSxfME732lVsHbjtrVN-gzGzoHsz', '6c6dhPrpfFwQLTCq1sh3Soco');

    //Prod config
    AV.initialize('HecuEekPMzfb9QfvBsyLju3S-gzGzoHsz', 'fgSVzrA8mYR5WPXlNl4Q288U');

  });

});


//page setting
angularModule.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('index', {
      cache: false,
      url: '/',
      templateUrl: "index.html"
    })
    .state('login', {
      cache: false,
      url: '/login',
      templateUrl: "login.html"
    })
    .state('userProfile', {
      cache: false,
      templateUrl: "userProfile.html",
      params: {
        people: null
      }
    })
    .state('netgativeUser', {
      cache: false,
      templateUrl: "searchNetgativeUser.html"
    })
    .state('search', {
      templateUrl: "searchUser.html"
    })
    .state('paymentApproval', {
      cache: false,
      templateUrl: "paymentApproval.html"
    })
    .state('userFee', {
      cache: false,
      templateUrl: "userFee.html",
      params: {
        people: null
      }
    })
    .state('userPayment', {
      cache: false,
      templateUrl: "userPayment.html",
      params: {
        people: null
      }
    })
    .state('collectFee', {
      cache: false,
      templateUrl: "collectFee.html",
      params: {
        people: null
      }
    })
    .state('createFee', {
      cache: false,
      templateUrl: "createFee.html",
      params: {
        people: null
      }
    })
    .state('exportData', {
      cache: false,
      templateUrl: "exportData.html",
    })

    //.state('test', {
    //  templateUrl: "test.html",
    //  //non URL para
    //  params: {
    //    test: "1234"
    //  },
    //  controller: function($scope, $stateParams) {
    //    $scope.a = $stateParams.test;
    //    alert($scope.a);
    //  }
    //})
    //.state('tabs.dash', {
    //  url: "/tab/dash",
    //  views: {
    //    'home-tab': {
    //      templateUrl: "templates/tab-dash.html"
    //    }
    //  }
    //})
    .state('home', {
      cache: false,
      templateUrl: "home.html"
    })


   $urlRouterProvider.otherwise("login");
});

angularModule.factory('Camera', ['$q', function ($q) {

  return {

      /** ---------------------------------------------------------------------
       *
       * @param options
       * @returns {*}
       *
       --------------------------------------------------------------------- */
      getPictureFromGallery: function (options) {
          var q = $q.defer();

          navigator.camera.getPicture(function (result) {
              // Do any magic you need
              q.resolve(result);
          }, function (err) {
              q.reject(err);
          }, options);

          return q.promise;
      },

      /** ---------------------------------------------------------------------
       *
       * @param options
       * @returns {*}
       *
       --------------------------------------------------------------------- */
      getPicture: function (options) {
          var q = $q.defer();

          navigator.camera.getPicture(function (result) {
              // Do any magic you need
              q.resolve(result);
          }, function (err) {
              q.reject(err);
          }, options);

          return q.promise;
      },
      /** ---------------------------------------------------------------------
       *
       * @param img_path
       * @returns {*}
       *
       --------------------------------------------------------------------- */
      resizeImage: function (img_path) {
          var q = $q.defer();
          window.imageResizer.resizeImage(function (success_resp) {
              console.log('success, img re-size: ' + JSON.stringify(success_resp));
              q.resolve(success_resp);
          }, function (fail_resp) {
              console.log('fail, img re-size: ' + JSON.stringify(fail_resp));
              q.reject(fail_resp);
          }, img_path, 200, 0, {
              imageDataType: ImageResizer.IMAGE_DATA_TYPE_URL,
              resizeType: ImageResizer.RESIZE_TYPE_MIN_PIXEL,
              pixelDensity: true,
              storeImage: false,
              photoAlbum: false,
              format: 'jpg'
          });

          return q.promise;
      },

      /** ---------------------------------------------------------------------
       *
       * @param img_path
       * @returns {*}
       *
       --------------------------------------------------------------------- */
      toBase64Image: function (img_path) {
          var q = $q.defer();

          if(img_path && window.imageResizer) {
            window.imageResizer.resizeImage(function (success_resp) {
              console.log('success, img toBase64Image: ' + JSON.stringify(success_resp));
              q.resolve(success_resp);
            }, function (fail_resp) {
              console.log('fail, img toBase64Image: ' + JSON.stringify(fail_resp));
              q.reject(fail_resp);
            }, img_path, 1, 1, {
              imageDataType: ImageResizer.IMAGE_DATA_TYPE_URL,
              resizeType: ImageResizer.RESIZE_TYPE_FACTOR,
              format: 'jpg'
            });
          }
          else {
            q.resolve('');
          }

          return q.promise;
      }
  }
}]);



