var angularModule =  angular.module('starter.loginController', ['ionic']);

angularModule.controller('loginController', function($scope, $ionicLoading, $ionicPopup, $state, $window) {

    $scope.loginName = '';
    $scope.pwd = '';

    $scope.autoLogin = function()
    {
        var username = window.localStorage.getItem("username");
        var password = window.localStorage.getItem("password");
        if(username && password)
        {
          $scope.loginName = username;
          $scope.pwd = password;
        }
    };

    $scope.loginPressed = function()
    {
        if($scope.loginName.length == 0 || $scope.pwd.length == 0)
      {
        $scope.showEmptyPassword();
        return;
      }

      $ionicLoading.show({
        template: 'Loading...'
      });

      AV.User.logIn($scope.loginName, $scope.pwd, {
        success: function(user) {

          if (AV.User._currentUser !== user) {
            AV.User.logOut();
          }
          user._isCurrentUser = true;
          AV.User._currentUser = user;
          AV.User._currentUserMatchesDisk = true;

          //save password
          if(typeof(Storage) != "undefined") {
              window.localStorage.setItem("username", $scope.loginName);
              window.localStorage.setItem("password", $scope.pwd);

              console.log("Data is stored, reload this page to retrieve it back!");
          } else {
              console.log("LocalStorage not supported!");
          }

          // Do stuff after successful login.
          $ionicLoading.hide();
          //$window.location.href = '/home.html';
          $state.go('home');
        },
        error: function(user, error) {
          // The login failed. Check error to see why.
          $ionicLoading.hide();
          $scope.loginFail();
        }
      });
    };

    $scope.showEmptyPassword = function() {
      var alertPopup = $ionicPopup.alert({
        title: 'Error',
        template: 'Please enter account name and password!'
      });
      alertPopup.then(function(res) {
      });
    };

    $scope.loginFail = function() {
      var alertPopup = $ionicPopup.alert({
        title: 'Error',
        template: 'Login fail!'
      });
      alertPopup.then(function(res) {
      });
    };

  });



